library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity contatore_impl1 is
  port(
      clk:  in std_logic;
      rst:  in std_logic;
      cont: out std_logic_vector(2 downto 0);
      slowclock: out std_logic

  );
end contatore_impl1;

architecture impl of contatore_impl1 is
  type state_type is (S0, S1, S2, S3, S4, S5, S6, S7, S8, S9);
  signal next_state, current_state: state_type;

begin

  state_reg: process(clk, rst)
  begin
    if rst='1' then
      current_state <= S0;
    elsif rising_edge(clk) then
      current_state <= next_state;
    end if;
  end process;
  
  delta_lambda: process(current_state)
  begin
    case current_state is
      when S0 =>
        next_state <= S1;
        cont <= "000";
        slowclock <= '0';
      when S1 =>
        next_state <= S2;
        cont <= "001";
        slowclock <= '0';
      when S2 =>
        next_state <= S3;
        cont <= "010";
        slowclock <= '0';
      when S3 =>
        next_state <= S4;
        cont <= "011";
        slowclock <= '0';
      when S4 =>
        next_state <= S5;
        cont <= "100";
        slowclock <= '0';
      when S5 =>
        next_state <= S6;
        cont <= "000";
        slowclock <= '1';
      when S6 =>
        next_state <= S7;
        cont <= "001";
        slowclock <= '1';
      when S7 =>
        next_state <= S8;
        cont <= "010";
        slowclock <= '1';
      when S8 =>
        next_state <= S9;
        cont <= "011";
        slowclock <= '1';
      when S9 =>
        next_state <= S0;
        cont <= "100";
        slowclock <= '1';
    end case;
  end process;

end impl;
    

