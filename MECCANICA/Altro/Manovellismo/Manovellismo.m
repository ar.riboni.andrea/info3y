%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Politecnico di Milano - Corso di Studi in Ingegneria Informatica
%
% Meccanica (per Ingegneria Informatica) - R.Corradi, A.Facchinetti
%
% Scheda di approfondimento n.2 :
% ANALISI CINEMATICA DEL MANOVELLISMO ORDINARIO CENTRATO
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Dati manovellismo
a=44.25e-3;                     % raggio di manovella [m]
b=157e-3;                       % lunghezza biella [m]
omega=5800;                     % regime di rotazione, costante [giri/minuto]

% SOLUZIONE
alfadeg=[0:1:359];              % vettore posizione angolare manovella [deg]
alfa=alfadeg*(pi/180);          % vettore posizione angolare manovella [rad]
alfap=omega*2*pi/60;            % velocit� angolare manovella [rad/s]
lambda=a/b;                     % rapporto lunghezza manovella/lunghezza biella

% analisi di posizione
c = a*cos(alfa) + b*sqrt(1-((a*sin(alfa))./b).^2); %posizione corsoio [m]
beta = asin(-(a*sin(alfa))./b); % angolo biella [rad]

% analisi di velocit�
cp=zeros(size(c));              % velocit� corsoio (inizializzo vettore) [m]
betap=zeros(size(c));           % velocit� angolare biella (inizializzo vettore) [m]
for ii = 1:length(alfa)         % ciclo sul vettore delle posizioni angolari
    A = [1   b*sin(beta(ii));
         0  -b*cos(beta(ii))];

    B = [-alfap*a*sin(alfa(ii));
          alfap*a*cos(alfa(ii))];

    xp = inv(A)*B;

    cp(ii)    = xp(1);
    betap(ii) = xp(2);
end

% analisi di accelerazione
cpp=zeros(size(c));             % accelerazione corsoio (inizializzo vettore) [m]
betapp=zeros(size(c));          % accelerazione angolare biella (inizializzo vettore) [m]
for ii = 1:length(alfa)         % ciclo sul vettore delle posizioni angolari
    A = [1   b*sin(beta(ii));
         0  -b*cos(beta(ii))];

    B = -[alfap^2*a*cos(alfa(ii)) + betap(ii)^2*b*cos(beta(ii));
          alfap^2*a*sin(alfa(ii)) + betap(ii)^2*b*sin(beta(ii))];

    xpp = inv(A)*B;

    cpp(ii)    = xpp(1);
    betapp(ii) = xpp(2);
end

% APPROSSIMAZIONE DEL PRIMO E DEL SECONDO ORDINE
% posizione
c1_approx=a*cos(alfa) + b;                                  % approssimazione del primo ordine
c2_approx=a*cos(alfa) + b + a*a/b/4*(cos(2*alfa)-1) ;       % approssimazione del secondo ordine

% velocit�
cp1_approx=a*alfap * ( -sin(alfa) );                        % approssimazione del primo ordine										
cp2_approx=a*alfap * ( -sin(alfa) -a/b/2*sin(2*alfa) );     % approssimazione del secondo ordine

% accelerazione
cpp1_approx=a* alfap^2 * ( -cos(alfa) );                    % approssimazione del primo ordine										
cpp2_approx=a* alfap^2 * ( -cos(alfa) -a/b*cos(2*alfa) );   % approssimazione del secondo ordine

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%GRAFICI
figure(1)
set(gcf,'position',[150 150 700 800])
subplot(311)
plot(alfadeg,c,'r',alfadeg,c1_approx,'g',alfadeg,c2_approx,'b'), grid on
ylabel(sprintf('Posizione piede di biella [m]'))
legend('Esatta','Primo ordine','Secondo ordine')
set(gca,'xlim',[0 360],'xtick',[0:90:360])
subplot(312)
plot(alfadeg,cp,'r',alfadeg,cp1_approx,'g',alfadeg,cp2_approx,'b'), grid on
ylabel(sprintf('Velocit� piede di biella [m/s]'))
set(gca,'xlim',[0 360],'xtick',[0:90:360])
subplot(313)
plot(alfadeg,cpp,'r',alfadeg,cpp1_approx,'g',alfadeg,cpp2_approx,'b'), grid on
ylabel(sprintf('Accelerazione piede di biella [m/s^2]'))
set(gca,'xlim',[0 360],'xtick',[0:90:360])
xlabel('Rotazione manovella [deg]')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ANIMAZIONE DEL MOTO
for jj=1:length(alfa) %realizzo il filmato passo passo
    xA=a*cos(alfa(jj));             %coordinate punto A
    yA=a*sin(alfa(jj));
    if jj==1 %primo frame
        figure(2)
        h1=plot([0 xA],[0 yA],'r'); grid on,hold on % manovella
        h2=plot([xA c(jj)],[yA 0],'g');             % biella
        h3=plot([0 xA c(jj)],[0 yA 0],'bo');        % punti sistema articolato
        axis equal
        set(gca,'xlim',[-1.1 1.1]*(a+b),'ylim',[-1.1 1.1]*a)
    else
        set(h1,'xdata',[0 xA],'ydata',[0 yA])
        set(h2,'xdata',[xA c(jj)],'ydata',[yA 0])
        set(h3,'xdata',[0 xA c(jj)],'ydata',[0 yA 0])
    end
    M(jj) = getframe;
end
movie(M,5,36) %mostra il filmato