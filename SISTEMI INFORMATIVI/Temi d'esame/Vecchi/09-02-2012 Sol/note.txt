I diagrammi di questa soluzione sono stati disegnati con StarUML.

Per tale motivo la notazione risulta in rari casi errata ma tuttavia di chiaro significato.

Per la notazione corretta da utilizzare si invita a fare riferimento a quella indicata
sul libro di testo e descritta ampiamente a lezione.